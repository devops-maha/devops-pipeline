# Spin up the Project

## 1. Fill in the Gitlab Variables:
  - **Container_Image_Url**: The URL of the container image e.g. "registry.gitlab.com/devops-maha/devops-pipeline"
  - **DB_PASSWORD**: The password for the database as kubernetes Config: 
    ```yaml
      apiVersion: v1
      kind: ConfigMap
      metadata:
      name: webservice-config
      data:
      config.conf:
      "XXXXXXXXX"
  - **Email**: Email Address for the cert manager
  - **KubeConfig**: The kubeconfig file for the cluster as yaml:
    ```yaml
    apiVersion: v1
    clusters:
    - cluster:
      certificate-authority-data: 
    server: 
      name: k8s_cluster
      contexts:
      - context:
        cluster: k8s_cluster
        user: k8s_cluster
        name: k8s_cluster
        current-context: k8s_cluster
        kind: Config
        preferences: {}
        users:
      - name: k8s_cluster
        user:
        client-certificate-data: 
    client-key-data: 
  - **Kubernetes_Namespace**: The namespace for the deployment
  - **LOGGLY_TOKEN**: The loggly token for the logging. Get it on loggly.com
  - **RedisConfig**: The redis config as yaml:
    ```yaml
    apiVersion: v1
    kind: ConfigMap
    metadata:
    name: redis-config
    data:
    redis-config: |
    requirepass XXXXXXX
    user webservice ~* &* +@all on >XXXXXXXX
  - **Webservice_URL**: URL of the Webservice
  - **PROM_URL**: URL of the Prometheus Service

## 2. Set up a Kubernetes Cluster:

- **Create a Kubernetes Cluster**:
- We have 2 Options:

  **Option 1**: 
  - Create a Kubernetes Cluster on a cloud provider of your choice
  - Get the kubeconfig file for the cluster and fill in the Gitlab Variable "KubeConfig"
  - Install traefik and Certmanager

  **Option 2**:
  - Go to [kubernetes](../kubernetes/readme.md) and fill out the template variable file
  - save it as tfvars.auto.tfvars
  - Run the terraform script to create the kubernetes cluster

    ```
    terraform plan
    terraform init
    terraform apply -auto-approve
    ```    


## 3. Spin up a Gitlab Runner:

 - Go to [gitlab-runner](../runner/readme.md) and fill out the template variable file
 - save it as tfvars.auto.tfvars
 - Run the terraform script to create the gitlab runner

    ```
    terraform plan
    terraform init
    terraform apply -auto-approve
    ```


## 4. Set the DNS Records for your Domain:
- Set the DNS Records for your domain to point to the LoadBalancer IP of the Traefik Service. You will get this as output from the terraform Kubernetes setup.
- I am using namecheap.com, and unfortunately it is not possible to use their api on the free domain. Thats why I have to do it manually beforehand.


## 5. Run the Pipeline:
- Commit either to the main branch or the dev branch
- The pipeline triggers on very commit
- For the productive enviroment you need to manually approve the deployment to kubernetes via the gitlab pipeline gui



# Information


- Installed Applications on the Kubernetes: Fluentd, Redis, Webservice, Prometheus, Grafana
- The pipeline always takes the latest stable image for the deployment. Latest dev image is tagged as latest-dev, and the tag latest is the newest from the main branch
- Grafana login:
  - Name: admin
  - Password: prom-operator


