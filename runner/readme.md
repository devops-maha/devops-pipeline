## Guide to deploy a EC2 Instance and register it as Gitlab Project runner


### Requirements:
    - Gitlab Personal Access Token
    - Terraform installed
    - AWS Credentials


### Setup
- Fill out the template variable file
- save it as tfvars.auto.tfvars
- Run the terraform script to create the EC2 Instance on aws and register it for your project as runner

  ```
  terraform plan
  terraform init
  terraform apply -auto-approve
  ```    

### Variables

access_key: AWS Access Key
secret_key: AWS Secret Key
session_token: AWS Session Token
gitlab_token: Gitlab token to change CI/CD Varibales. Get it [here](https://gitlab.com/-/user_settings/personal_access_tokens)