resource "aws_instance" "gitlab_runner" {
  depends_on = [gitlab_user_runner.gitlab_runner]
  instance_type = "t3.medium"
  tags = {
    Name = "gitlab-runner"
  }
  ami = "ami-06f8dce63a6b60467"


  user_data = templatefile("install_runner.sh",{
    "registration_token" = gitlab_user_runner.gitlab_runner.token
})


  vpc_security_group_ids = [
    aws_security_group.allow_ssh.id,
    aws_security_group.allow_outbound.id
  ]

}

resource "aws_security_group" "allow_ssh" {

  name="allow_ssh"
  ingress {
    from_port = 22
    to_port=22
    protocol="tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }




}


resource "aws_security_group" "allow_outbound" {
  name="allow_outbound"
  egress {
    from_port = 0
    to_port=0
    protocol="-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }
}

resource "gitlab_user_runner" "gitlab_runner" {
  runner_type = "project_type"
  tag_list = ["docker-privileged"]
  project_id = 56633909

}





