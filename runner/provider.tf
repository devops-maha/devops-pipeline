
terraform {
  required_version = ">= 1"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.44.0"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "16.10.0"
    }
  }
}


provider "aws" {
  region = var.aws_region
  access_key = var.access_key
  secret_key = var.secret_key
  token = var.session_token
}


provider "gitlab" {
  token = var.gitlab_token
}

