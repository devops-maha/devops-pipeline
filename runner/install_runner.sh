#! /bin/bash
sudo apt-get update


#install Docker
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh

# Download the binary for your system
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

# Give it permission to execute
sudo chmod +x /usr/local/bin/gitlab-runner

# Create a GitLab Runner user
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# Install and run as a service
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start


sudo gitlab-runner register  --url https://gitlab.com/  --token ${registration_token}  --non-interactive  --name gitlab_runner  --executor docker  --docker-image alpine:latest  --docker-volumes /var/run/docker.sock:/var/run/docker.sock

sudo systemctl restart gitlab-runner
sudo gitlab-runner run