workflow:
  rules:
    - when: always

variables:
  version: 0.0.$CI_PIPELINE_ID


stages:
  - build-artifact
  - build-docker-image
  - test
  - publish
  - deploy


.production-template-manual:
  environment:
    name: production
    url: https://$Webservice_URL
    deployment_tier: production
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
      when: manual

.production-template-auto:
  environment:
    name: production
    url: https://$Webservice_URL
    deployment_tier: production
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
      when: always

.dev-template:
  environment:
    name: development
    url: https://$Webservice_URL
    deployment_tier: development
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
      when: never
    - when: always

build-artifact:
  stage: build-artifact
  image: registry.hub.docker.com/library/golang:1.21.4
  script:
    - echo "Get all the dependencies"
    - go get -t ./...
    - echo "Building the artifact"
    - env GOOS=linux GOARCH=amd64 go build -o ./artifact.bin -ldflags "-X webservice/configuration.version=${version}" ./*.go
    - echo "Building complete."
  artifacts:
    paths:
      - ./artifact.bin
    expire_in: 1 days


.build-docker-image:
  stage: build-docker-image
  image: docker:latest
  dependencies:
    - build-artifact
  needs:
    - build-artifact
  tags:
    - docker-privileged
  services:
    - docker:dind

build-docker-image-prod:
  extends:
    - .build-docker-image
    - .production-template-auto
  script:
    - echo "Building the docker image"
    - docker build -t $CI_REGISTRY_IMAGE:${version} .
    - echo "Pushing the docker image"
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - echo "Tag the image with release"
    - docker tag $CI_REGISTRY_IMAGE:${version} $CI_REGISTRY_IMAGE:latest
    - docker push $CI_REGISTRY_IMAGE:latest
    - docker push $CI_REGISTRY_IMAGE:${version}
    - echo "Pushed the docker images"

build-docker-image-dev:
  extends:
    - .build-docker-image
    - .dev-template
  script:
    - echo "Building the docker image"
    - docker build -t $CI_REGISTRY_IMAGE:${version} .
    - echo "Pushing the docker image"
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - echo "Tag the image with release"
    - docker tag $CI_REGISTRY_IMAGE:${version} $CI_REGISTRY_IMAGE:latest-dev
    - docker push $CI_REGISTRY_IMAGE:latest-dev
    - docker push $CI_REGISTRY_IMAGE:${version}
    - echo "Pushed the docker images"


unit-test:   # This job runs in the test stage.
  stage: test    # It only starts when the job in the build stage completes successfully.
  image: registry.hub.docker.com/library/golang:1.21.4
  script:
    - echo "Running unit tests..."
    - go test -race -v ./...
    - echo "Tests completed..."



publish-artifact:
  stage: publish
  image: registry.hub.docker.com/library/ubuntu:latest
  tags:
    - docker-privileged
  dependencies:
    - build-artifact
  before_script:
    - apt update && apt install -y ca-certificates curl
    - update-ca-certificates
  script:
    - |
      curl \
        --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
        --upload-file ./artifact.bin \
        "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/artifacts/${version}/webservice"


deploy-kubernetes-prod:
  stage: deploy
  image: registry.hub.docker.com/library/ubuntu:latest
  tags:
    - docker-privileged
  before_script:
    - apt-get update && apt-get install -y gettext-base
    - apt update && apt install -y ca-certificates curl
    - update-ca-certificates
    - curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
    - chmod +x kubectl
    - mv kubectl /usr/local/bin
    - export KUBECONFIG=$KubeConfig
    - curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 > get_helm.sh
    - chmod 700 get_helm.sh
    - ./get_helm.sh
  script:
    - envsubst < webservice.yaml > webservice-prod.yaml
    - envsubst < redis.yaml > redis-prod.yaml
    - envsubst < certificate.yaml > certificate-prod.yaml
    - envsubst < metrics.yaml > metrics-filled.yaml
    - kubectl config set-context --current --namespace=$Kubernetes_Namespace
    - kubectl create secret generic loggly-config --from-literal=loggly.token=$LOGGLY_TOKEN --dry-run=client -o yaml | kubectl apply -f -
    - kubectl apply -f $DB_PASSWORD
    - kubectl apply -f ./certificate-prod.yaml
    - kubectl apply -f ./webservice-prod.yaml
    - kubectl apply -f $RedisConfig
    - kubectl apply -f ./redis-prod.yaml
    - kubectl apply -f ./fluentd.yaml
    - kubectl apply --server-side -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/v0.73.0/example/prometheus-operator-crd/monitoring.coreos.com_alertmanagerconfigs.yaml
    - kubectl apply --server-side -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/v0.73.0/example/prometheus-operator-crd/monitoring.coreos.com_alertmanagers.yaml
    - kubectl apply --server-side -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/v0.73.0/example/prometheus-operator-crd/monitoring.coreos.com_podmonitors.yaml
    - kubectl apply --server-side -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/v0.73.0/example/prometheus-operator-crd/monitoring.coreos.com_probes.yaml
    - kubectl apply --server-side -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/v0.73.0/example/prometheus-operator-crd/monitoring.coreos.com_prometheusagents.yaml
    - kubectl apply --server-side -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/v0.73.0/example/prometheus-operator-crd/monitoring.coreos.com_prometheuses.yaml
    - kubectl apply --server-side -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/v0.73.0/example/prometheus-operator-crd/monitoring.coreos.com_prometheusrules.yaml
    - kubectl apply --server-side -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/v0.73.0/example/prometheus-operator-crd/monitoring.coreos.com_scrapeconfigs.yaml
    - kubectl apply --server-side -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/v0.73.0/example/prometheus-operator-crd/monitoring.coreos.com_servicemonitors.yaml
    - kubectl apply --server-side -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/v0.73.0/example/prometheus-operator-crd/monitoring.coreos.com_thanosrulers.yaml
    - helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
    - helm repo update
    - helm upgrade -i prometheus prometheus-community/kube-prometheus-stack --namespace prometheus --create-namespace
    - kubectl apply -f ./metrics-filled.yaml
  extends:
    - .production-template-manual

deploy-kubernetes-dev:
  stage: deploy
  image: registry.hub.docker.com/library/ubuntu:latest
  tags:
    - docker-privileged
  before_script:
    - apt-get update && apt-get install -y gettext-base
    - apt update && apt install -y ca-certificates curl
    - update-ca-certificates
    - curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
    - chmod +x kubectl
    - mv kubectl /usr/local/bin
    - export KUBECONFIG=$KubeConfig
    - curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 > get_helm.sh
    - chmod 700 get_helm.sh
    - ./get_helm.sh
  script:
    - envsubst < webservice.yaml > webservice-dev.yaml
    - envsubst < redis.yaml > redis-dev.yaml
    - envsubst < certificate.yaml > certificate-dev.yaml
    - envsubst < metrics.yaml > metrics-filled.yaml
    - kubectl config set-context --current --namespace=$Kubernetes_Namespace
    - kubectl create secret generic loggly-config --from-literal=loggly.token=$LOGGLY_TOKEN --dry-run=client -o yaml | kubectl apply -f -
    - kubectl apply -f $DB_PASSWORD
    - kubectl apply -f ./certificate-dev.yaml
    - kubectl apply -f ./webservice-dev.yaml
    - kubectl apply -f $RedisConfig
    - kubectl apply -f ./redis-dev.yaml
    - kubectl apply -f ./fluentd.yaml
    - kubectl apply --server-side -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/v0.73.0/example/prometheus-operator-crd/monitoring.coreos.com_alertmanagerconfigs.yaml
    - kubectl apply --server-side -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/v0.73.0/example/prometheus-operator-crd/monitoring.coreos.com_alertmanagers.yaml
    - kubectl apply --server-side -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/v0.73.0/example/prometheus-operator-crd/monitoring.coreos.com_podmonitors.yaml
    - kubectl apply --server-side -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/v0.73.0/example/prometheus-operator-crd/monitoring.coreos.com_probes.yaml
    - kubectl apply --server-side -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/v0.73.0/example/prometheus-operator-crd/monitoring.coreos.com_prometheusagents.yaml
    - kubectl apply --server-side -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/v0.73.0/example/prometheus-operator-crd/monitoring.coreos.com_prometheuses.yaml
    - kubectl apply --server-side -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/v0.73.0/example/prometheus-operator-crd/monitoring.coreos.com_prometheusrules.yaml
    - kubectl apply --server-side -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/v0.73.0/example/prometheus-operator-crd/monitoring.coreos.com_scrapeconfigs.yaml
    - kubectl apply --server-side -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/v0.73.0/example/prometheus-operator-crd/monitoring.coreos.com_servicemonitors.yaml
    - kubectl apply --server-side -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/v0.73.0/example/prometheus-operator-crd/monitoring.coreos.com_thanosrulers.yaml
    - helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
    - helm repo update
    - helm upgrade -i prometheus prometheus-community/kube-prometheus-stack --namespace prometheus --create-namespace
    - kubectl apply -f ./metrics-filled.yaml
  extends:
    - .dev-template














