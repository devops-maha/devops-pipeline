FROM ubuntu:latest

COPY --chmod=0755 ./artifact.bin /bin/webservice


ENV HOST 0.0.0.0
ENV PORT 8080


ENTRYPOINT ["/bin/webservice"]
CMD [ "" ]