resource "helm_release" "certmanager" {
  chart = "cert-manager"
  name  = "certmanager"
  repository = "https://charts.jetstack.io"

  create_namespace = true
  namespace        = "cert-manager"

  set {
    name  = "installCRDs"
    value = "true"
  }
}