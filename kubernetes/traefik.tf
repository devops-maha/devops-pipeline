resource "helm_release" "traefik" {
  name = "traefik"

  repository = "https://helm.traefik.io/traefik"
  chart      = "traefik"

  create_namespace = true
  namespace        = "traefik"

  set {
    type  = "string"
    name  = "service.annotations.kubernetes\\.civo\\.com/firewall-id"
    value = civo_firewall.fw_1.id
  }
  set {
    name  = "ingressClass.enabled"
    value = "true"
  }
  set {
    name  = "ingressClass.isDefaultClass"
    value = "true"
  }

  set {
    name  = "ports.web.redirectTo.port"
    value = "websecure"
  }

  set {
    name  = "ports.websecure.tls.enabled"
    value = "true"
  }
}