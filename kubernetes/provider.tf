terraform {
  required_version = ">= 0.13.0"

  required_providers {
    civo = {
      source  = "civo/civo"
      version = "1.0.39"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "2.13.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.27.0"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = "1.14.0"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "16.10.0"
    }



  }
}

variable "civo_token" {
  type = string
}



provider "civo" {
  token  = var.civo_token
  region = "FRA1"

}

provider "helm" {
  kubernetes {
    host                   = "${yamldecode(civo_kubernetes_cluster.k8s_cluster.kubeconfig).clusters.0.cluster.server}"
    client_certificate     = "${base64decode(yamldecode(civo_kubernetes_cluster.k8s_cluster.kubeconfig).users.0.user.client-certificate-data)}"
    client_key             = "${base64decode(yamldecode(civo_kubernetes_cluster.k8s_cluster.kubeconfig).users.0.user.client-key-data)}"
    cluster_ca_certificate = "${base64decode(yamldecode(civo_kubernetes_cluster.k8s_cluster.kubeconfig).clusters.0.cluster.certificate-authority-data)}"
  }
}

provider "kubernetes" {
  host                   = "${yamldecode(civo_kubernetes_cluster.k8s_cluster.kubeconfig).clusters.0.cluster.server}"
  client_certificate     = "${base64decode(yamldecode(civo_kubernetes_cluster.k8s_cluster.kubeconfig).users.0.user.client-certificate-data)}"
  client_key             = "${base64decode(yamldecode(civo_kubernetes_cluster.k8s_cluster.kubeconfig).users.0.user.client-key-data)}"
  cluster_ca_certificate = "${base64decode(yamldecode(civo_kubernetes_cluster.k8s_cluster.kubeconfig).clusters.0.cluster.certificate-authority-data)}"
}

provider "kubectl" {
  host                   = "${yamldecode(civo_kubernetes_cluster.k8s_cluster.kubeconfig).clusters.0.cluster.server}"
  client_certificate     = "${base64decode(yamldecode(civo_kubernetes_cluster.k8s_cluster.kubeconfig).users.0.user.client-certificate-data)}"
  client_key             = "${base64decode(yamldecode(civo_kubernetes_cluster.k8s_cluster.kubeconfig).users.0.user.client-key-data)}"
  cluster_ca_certificate = "${base64decode(yamldecode(civo_kubernetes_cluster.k8s_cluster.kubeconfig).clusters.0.cluster.certificate-authority-data)}"
  load_config_file       = false
}

provider "gitlab" {
  token = var.gitlab_token
}

variable "gitlab_token" {
  type = string
}