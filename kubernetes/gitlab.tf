resource "gitlab_project_variable" "KubeConfig" {
  key     = "KubeConfig"
  project =  56633909
  value   = civo_kubernetes_cluster.k8s_cluster.kubeconfig
  variable_type = "file"
  depends_on = [civo_kubernetes_cluster.k8s_cluster]
}