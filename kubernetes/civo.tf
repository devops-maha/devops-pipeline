data "civo_size" "xsmall" {
  filter {
    key="name"
    values = ["g4s.kube.small"]
    match_by = "re"
  }
}




resource "civo_kubernetes_cluster" "k8s_cluster" {
  name = "k8s_cluster"
  applications = "rancher"
  kubernetes_version = "1.29.2-k3s1"

  pools {
    node_count = 5
    size       = element(data.civo_size.xsmall.sizes, 0).name
  }
  firewall_id = civo_firewall.fw_1.id
}

resource "civo_firewall" "fw_1" {
  name = "fw_1"
  create_default_rules = false
  ingress_rule {
    action = "allow"
    cidr = ["0.0.0.0/0"]
    protocol = "tcp"
    port_range = "80"
    label = "kubernetes_http"
  }
  ingress_rule {
    action = "allow"
    cidr = ["0.0.0.0/0"]
    protocol = "tcp"
    port_range = "443"
    label = "kubernetes_https"
  }
  ingress_rule {
    protocol = "tcp"
    port_range = "6443"
    action      = "allow"
    cidr        = ["0.0.0.0/0"]
    label = "kubernetes_api"
  }

}

output "kubeconfig" {
  value = civo_kubernetes_cluster.k8s_cluster.kubeconfig
  sensitive = true

}

output "cluster_ip" {
    value = data.civo_loadbalancer.traefik_lb.public_ip
}

data "civo_loadbalancer" "traefik_lb" {

  depends_on = [
  helm_release.traefik
  ]

  name="k8s_cluster-traefik"
}

