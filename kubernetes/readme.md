## Guide to deploy a Kubernetes Cluster to civo.com with TerraForm


### Requirements: 
    - Civo.com API Token
    - Gitlab Personal Access Token
    - Terraform installed

### Setup
- Fill out the template variable file
- save it as tfvars.auto.tfvars
- Run the terraform script to create the kubernetes cluster

  ```
  terraform plan
  terraform init
  terraform apply -auto-approve
  ```    
  
### Variables

civo_token: Civo Token. Get it [here](https://dashboard.civo.com/security)<br>
gitlab_token: Gitlab token to change CI/CD Varibales. Get it [here](https://gitlab.com/-/user_settings/personal_access_tokens)